<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bizhostvn' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`_5JK<hrD@7)42R8mJII=FAk]UUwl5$YMYd}kLAw?;9 _Z`Mu8PaRa)Zk4P1i}>*' );
define( 'SECURE_AUTH_KEY',  '`SqHwtBkq|QJ9-o0)q<zKs+aThziI@O2!HI]*.kLvcPDvsoX 9$ke$cmi^2Gi<IO' );
define( 'LOGGED_IN_KEY',    'epu~WR.-e:|n7vP7Sl3B^BC0mzSit.OXD{w J6a%0%+O2|iz M6+t$am9>qoqaG)' );
define( 'NONCE_KEY',        ';jLy!FmggO$iEeq?Yw%n}Y+A$3gx`fz-X#v`p*DjOVuZtfLtBQu%OayXh)}*oYN6' );
define( 'AUTH_SALT',        'NQz75#)*.HE)DtL**-JA2Q$q4Os 0edIi=JH0GS]XS:3[(qvjb^(P1?^o#<a+p%n' );
define( 'SECURE_AUTH_SALT', 't@_+[j#aFaqG05<_pjlDsK;bRmi5UI:z=KUXK*=YYgOxYYfrDrG#>.i%!/@cn-59' );
define( 'LOGGED_IN_SALT',   'JCN`F_o`AKQ>.Aqcw5e,bzjMFD6MWLah<T 3a29n-/ed%zYi6``JB!z5cT6[c1RG' );
define( 'NONCE_SALT',       '1r-lvb%QT:+QFL,ck.2%O)iLoV|iC%&sZ.<<e(tw4fpwX97@Cx o^+1N|>Jn5U=>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
